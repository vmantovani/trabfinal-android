# Trabalho Final Android

Software desenvolvido visando o Trabalho Final da disciplina de Dispositivos Móveis. Professor: Diego Stiehl

## Aplicação

O software consiste em um App de Empréstimos. A aplicação visa realizar empréstimos de qualquer coisa, não tem algo específico. Determinada pessoa solicita o que quer emprestar, e a pessoa o qual estará emprestando o item, irá cadastrar esse empréstimo na aplicação. Tendo assim um controle dos itens emprestados. Os dados serão armazanados no banco local.

Aplicação iŕa contar com os seguintes requisitos:

* Cadastrar Pessoa
* Cadastrar Empréstimo
* Visualizar Empréstimo
* Visualizar Histórico Empréstimos

<!--## Pré requisitos-->

<!--* Sistema Operacional Linux;-->
<!--* Linguagem Ruby instalada junto com o framework Ruby On Rails;-->
<!--* O Software irá contar com a IDE Atom para seu desenvolvimento;-->
<!--* Ferramenta Astah, caso necessite de modelagem dos requisitos.-->
<!--* Banco de Dados Mysql. -->

<!--## Instalação-->

<!--Para instalação do ruby basta usar o seguinte comando:-->

<!--**sudo apt-get install ruby-full**-->

<!--Para saber a versão  e se a instalação deu certo basta usar o seguinte mando:-->

<!--**ruby -v**-->

<!--Diante disso, para instalar o ruby on rails, utilize os seguintes comandos:-->

<!--- **gem install rails --no-ri --no-rdoc**-->

<!--- **sudo apt-get install nodejs**-->

<!--Para Saber se deu certo a instalação, execute o seguinte comando:-->

<!--**rails -v**-->

## Elaborado com

* [Kotlin](https://kotlinlang.org/) - Linguagem utilizada.
* [Android Studio](https://developer.android.com/studio) - IDE utilizado.
* [Sqlite](https://www.sqlite.org) - Banco de Dados utilizado.


## Autor

**Vanderson Mantovani** - [GitLab](https://gitlab.com/vmantovani/trabfinal-android.git)



